<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PUG_TWIG
 */

?>

<h1><?php the_title() ?></h1>
<?php the_content() ?>