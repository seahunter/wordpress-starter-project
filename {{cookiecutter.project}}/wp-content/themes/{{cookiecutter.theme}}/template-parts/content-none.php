<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PUG_TWIG
 */

?>

<h1><?php esc_html_e( 'Nothing Found', '{{cookiecutter.theme}}' ); ?></h1>